We sampled squamate reptiles using live-trapping techniques. We used drift fence trapping arrays composed of four pitfall (19 L buckets) traps and six funnel traps positioned along three 6 m lengths of drift fence at 0, 120, and 240 degrees from center pitfall trap. Trapping arrays are spaced at least 250 m apart and not within 25 m of edge (similar to Bateman and Ostoja 2012). Arthropods are tallied in one pitfall trap per array and identified to the Order-level. Crews checked traps daily during reptile active season March to October from 2010 to 2019. We classified reptiles to species and assigned unique marks as in Waichman (1992) to identify individual lizards. To reduce data-entry errors, we recorded data using a mobile application designed by Bateman et al. (2013) for capture-mark-recapture studies.

Bateman, H. L., Lindquist, T. E., Whitehouse, R., & Gonzalez, M. M. (2013). Mobile application for wildlife capture–mark–recapture data collection and query. Wildlife Society Bulletin, 37(4), 838–845.

Bateman, H. L., & Ostoja, S. M. (2012). Invasive woody plants affect the composition of native lizard and small mammal communities in riparian woodlands. Animal Conservation, 15(3), 294–304.

Waichman, A. V. (1992). An alphanumeric code for toe clipping amphibians and reptiles. Herpetological Review., 23, 19–21.
