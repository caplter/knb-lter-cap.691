# knb-lter-cap.691

Long-term monitoring of reptiles and ground arthropods near the Phoenix-Mesa Gateway Airport.

## knb-lter-cap.691.3

- This is mostly a data refresh, adding information into 2022.
- However, this version also addresses a previously undetected problem in which counts of PSUE and MICRO/NEUR were excluded from the 2010-2019 data in previous versions. These fixes are detailed in commits [3d50e133](https://gitlab.com/caplter/knb-lter-cap.691/-/commit/3d50e133cde26e7df95d9c207d991b5f5746a503) and [2e7c895e](https://gitlab.com/caplter/knb-lter-cap.691/-/commit/2e7c895e94a529c890613d0c8abaec04d7637c02). The original data processing-script (in `archive/`) was updated to capture these previously omitted counts. This should be a one-time operation as PSUE and MICRO/NEUR will be reflected when downloading existing data during data refreshes. 
- In addition, data are now sorted by date for more intuitive navigation.
